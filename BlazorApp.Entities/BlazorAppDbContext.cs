﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BlazorApp.Entities
{
    public partial class BlazorAppDbContext : DbContext
    {
        //public BlazorAppDbContext()
        //{
        //}

        public BlazorAppDbContext(DbContextOptions<BlazorAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbUser> TbUser { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=DbTranApp;Username=postgres;Password=12345678;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.ActivationLink)
                    .HasName("TbUser_ActivationLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("TbUser_Email_key")
                    .IsUnique();

                entity.HasIndex(e => e.ForgotPasswordLink)
                    .HasName("TbUser_ForgotPasswordLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("TbUser_Username_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
